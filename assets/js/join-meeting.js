function joinMeetingAjax() {
  let data = {
    action: 'join_meeting',
    security : MyAjax.security
  };

  var meeting_url = false;

  jQuery.ajax({
    url: MyAjax.join_zoom_meeting_ajaxurl,
    type: 'POST',
    data: data,
    success: function(found_meeting) {
      meeting_url = found_meeting;
    }
  }).always(function(){
    processAjaxResponse(meeting_url);
  });
}

function processAjaxResponse(meeting_url) {
  if(meeting_url) {
    removeWaitingMessage();

    if(userCalledButton) {
      window.location.href = meeting_url;
      jQuery('.direct-join-zoom-meeting').remove();
      showJoinMeetingButton();
    }
    else {
      showMessageMeetingFound();
      showDirectJoinMeetingButton(meeting_url);
    }
  }
  else {
    userCalledButton = false;

    sleep(20000).then(() => {
      joinMeetingAjax();
    });
  }
}

function showDirectJoinMeetingButton(meeting_url) {
  var buttonClone = jQuery(getButtonObject()[0])
    .clone()
    .removeClass('join-zoom-meeting')
    .html('ASSISTIR AGORA')
    .attr({
      href: meeting_url,
      target: '_blank',
      onclick: 'restoreOriginalJoinMeetingButton()'
    })
    .show();

  jQuery('.direct-join-zoom-meeting').append(buttonClone);
}

function hideJoinMeetingButton() {
  getButtonObject().hide();
}

function showJoinMeetingButton() {
  getButtonObject().show();
}

function showWaitingMessage() {
  jQuery('.direct-join-zoom-meeting')
    .load('/wp-content/plugins/zoom-api-integration/assets/templates/waiting-message.html');
}

function removeWaitingMessage() {
  jQuery('.waiting-message').remove();
}

function showMessageMeetingFound() {
  jQuery('.direct-join-zoom-meeting')
    .html('<p>Encontramos uma sala disponível! Clique no botão abaixo para assistir à aula.</p>');
}

function restoreOriginalJoinMeetingButton() {
  jQuery('.direct-join-zoom-meeting').remove();
  showJoinMeetingButton();
}

function getButtonObject() {
  return jQuery('.join-zoom-meeting');
}

function sleep (time) {
  return new Promise((resolve) => setTimeout(resolve, time));
}

var userCalledButton;

jQuery(document).ready(function(){
  let join_meeting_button = getButtonObject();

  join_meeting_button.click(function(e){
    e.preventDefault();
    userCalledButton = true;
    getButtonObject().parent().append('<div class="direct-join-zoom-meeting"></div>');
    hideJoinMeetingButton();
    showWaitingMessage();
    joinMeetingAjax();
  });
});