<?php
class ZoomAPIIntegration {
	protected static $instance = null;

	public function __construct() {
		$this->updatePlugin();

		require_once( "ZoomAPI.php" );

		add_filter( 'zoom_manage_member_to_queue', array( $this, 'manageMemberToQueue' ), 10, 0 );
	}

	public static function get_instance() {
		if ( null === self::$instance ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	private function updatePlugin() {
		add_action( 'plugins_loaded', array( 'ZoomAPIIntegrationInstall', 'update_db_check' ) );
	}

	public function manageMemberToQueue(){
		global $ZoomAPI;

		return $ZoomAPI->manageMemberToQueue();
	}

	public static function redirectToZoomMeetingOrAddToQueue() {
		echo apply_filters( 'zoom_manage_member_to_queue', [] );
	}
}

ZoomAPIIntegration::get_instance();