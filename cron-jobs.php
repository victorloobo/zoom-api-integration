<?php
function add_every_minute_schedule( $schedules ) {
  $schedules[ 'every-minute' ] = array( 'interval' => 60, 'display' => 'Every minute' );
  return $schedules;
}
add_filter( 'cron_schedules', 'add_every_minute_schedule' );

function updateMeetingParticipantsFromAPI() {
  global $ZoomAPI;
  $ZoomAPI->updateMeetingParticipantsFromAPI();
}
add_action('update_meeting_participants_number', 'updateMeetingParticipantsFromAPI');

function create_cron_jobs() {
  if (! wp_next_scheduled ( 'update_meeting_participants_number' )) {
    wp_schedule_event(time(), 'every-minute', 'update_meeting_participants_number');
  }
}

function delete_cron_jobs() {
  wp_clear_scheduled_hook('update_meeting_participants_number');
}