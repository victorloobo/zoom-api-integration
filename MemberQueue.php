<?php
global $MemberQueue;
$MemberQueue = new MemberQueue();

class MemberQueue {
  public function manageMemberToQueue() {
    global $ZoomAPI;

    $current_user_id = wp_get_current_user()->data->ID;

    $member_in_queue = $this->showMemberToQueue($current_user_id);

    if($member_in_queue){
      $now = new DateTime();
      $member_next_update_date = DateTime::createFromFormat('Y-m-d H:i:s', $member_in_queue->update_date)->modify("+20 seconds");
      
      if($now <= $member_next_update_date) return false;
    }
    
    $this->updateMemberQueue($current_user_id);
    $zoom_meeting = $ZoomAPI->findAvailableMeetingRoom();

    if(!$zoom_meeting) {
      if(!$member_in_queue) $this->addMemberToQueue($current_user_id);

      return false;
    }

    if($member_in_queue) $this->removeMemberToQueue($current_user_id);

    return $zoom_meeting;
  }

  private function showMemberToQueue($user_id) {
    global $wpdb;
    $table_name = $wpdb->prefix . ZOOM_MEETING_QUEUE_TABLE;

    $result = $wpdb->get_row(sprintf("
      SELECT *
      FROM $table_name
      WHERE user_id = '%s'", $user_id
    ));

    return $result;
  }

  private function addMemberToQueue($user_id) {
    global $wpdb;
    $table_name = $wpdb->prefix . ZOOM_MEETING_QUEUE_TABLE;
    $now = Utils::currentDateTimeAsSting();

    $wpdb->query( $wpdb->prepare(
      "INSERT INTO $table_name (user_id, update_date)
      VALUES(%s, %s)", $user_id, $now
    ) );
  }

  private function updateMemberQueue($user_id) {
    global $wpdb;
    $table_name = $wpdb->prefix . ZOOM_MEETING_QUEUE_TABLE;
    $now = Utils::currentDateTimeAsSting();

    $wpdb->query( $wpdb->prepare(
      "UPDATE $table_name
      SET update_date = %s
      WHERE user_id = '%s'", $now, $user_id
    ) );
  }

  private function removeMemberToQueue($user_id) {
    global $wpdb;
    $table_name = $wpdb->prefix . ZOOM_MEETING_QUEUE_TABLE;

    $wpdb->query( $wpdb->prepare(
      "DELETE FROM $table_name
      WHERE user_id = '%s'", $user_id
    ) );
  }
}