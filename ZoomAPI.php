<?php
require_once(plugin_dir_path(__FILE__) . 'MemberQueue.php');

global $ZoomAPI;
$ZoomAPI = new ZoomAPI();

class ZoomAPI {
  const ZOOM_API_BASE_URL = 'https://api.zoom.us/v2';

  private $args;
  private $endpoint;

  public function findAvailableMeetingRoom() {
    $meeting = $this->getMeetingRoomData();

    return empty($meeting) ? false : $meeting->zoom_meeting_url;
  }

  public function manageMemberToQueue() {
    global $MemberQueue;

    return $MemberQueue->manageMemberToQueue();
  }

  public function updateMeetingParticipantsFromAPI() {
    $apis_data = $this->getActiveMeetings();

    $tokens = $this->generateTokens($apis_data);

    $meetings_ids = array_map(function($meeting){
      return $meeting->zoom_meeting_id;
    }, $apis_data);

    $result = [];

    foreach($tokens as $token) {
      $result = array_merge(array_values($result), array_values($this->makeRequest($token)));
    }

    if($result) {
      $live_meetings = array_filter($result, function($live_meeting) use($meetings_ids){
        return in_array($live_meeting->id, $meetings_ids);
      });

      if($live_meetings) $this->updateMeetingParticipantsByZoomMeetingId($live_meetings);
    }
  }

  private function getMeetingRoomData() {
    $meeting = $this->getMeetingNotFull();

    if($meeting) $this->updateMeetingData($meeting->id, ($meeting->participants + 1));

    return $meeting;
  }

  private function makeRequest($token) {
    $this->setArgs($token);

    $request = wp_remote_get( self::ZOOM_API_BASE_URL . $this->endpoint, $this->args );

    if( is_wp_error( $request ) || $request['response']['code'] != 200 ) return [];

    $body = wp_remote_retrieve_body( $request );

    $data = json_decode( $body );

    return $data ? $data->meetings : $data;
  }

  private function setArgs($token) {
    $this->args = [
      'headers' => [
        'Authorization' => "Bearer {$token}",
      ],
    ];

    $params = [
      'type' => 'live',
      'page_size' => 300,
    ];

    $params = http_build_query($params);

    $this->endpoint = "/metrics/meetings?{$params}";
  }

  private function generateTokens($apis_data) {
    $tokens = [];

    foreach($apis_data as $api_data) {
      if(!isset($tokens[$api_data->api_key])) {
        $tokens[$api_data->api_key] = Utils::generateJWTToken($api_data->api_key, $api_data->secret_key);
      }
    }

    return $tokens;
  }

  private function getActiveMeetings() {
    global $wpdb;

    $table_name = $wpdb->prefix . ZOOM_API_INTEGRATION_TABLE;
  
    return $wpdb->get_results("
      SELECT *
      FROM $table_name
      WHERE active_meeting IS TRUE
      ORDER BY
        participants DESC,
        update_date ASC"
    );
  }

  private function getMeetingNotFull() {
    global $wpdb;

    $table_name = $wpdb->prefix . ZOOM_API_INTEGRATION_TABLE;
  
    return $wpdb->get_row("
      SELECT *
      FROM $table_name
      WHERE participants < participants_capacity
      AND active_meeting IS TRUE
      ORDER BY
        participants ASC,
        update_date ASC"
    );
  }

  private function updateMeetingData($id, $participants) {
    global $wpdb;
    $table_name = $wpdb->prefix . ZOOM_API_INTEGRATION_TABLE;
    $now = Utils::currentDateTimeAsSting();

    return $wpdb->query( $wpdb->prepare(
      "UPDATE $table_name
      SET
        update_date = %s,
        participants = %s
      WHERE id = %s", $now, $participants, $id
    ) );
  }

  private function updateMeetingParticipantsByZoomMeetingId($live_meetings) {
    global $wpdb;
    $table_name = $wpdb->prefix . ZOOM_API_INTEGRATION_TABLE;
    $now = Utils::currentDateTimeAsSting();
    
    $case_sql = 'CASE ';
    foreach($live_meetings as $live_meeting){
      $case_sql .= "WHEN zoom_meeting_id = {$live_meeting->id} THEN {$live_meeting->participants} ";
    }
    $case_sql .= "END";

    $zoom_meeting_ids = implode(', ', array_column($live_meetings, 'id'));

    return $wpdb->query( $wpdb->prepare(
      "UPDATE $table_name
      SET
        update_date = %s,
        participants = ($case_sql)
      WHERE zoom_meeting_id IN ($zoom_meeting_ids)", $now
    ) );
  }
}