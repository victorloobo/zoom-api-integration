<?php
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}

define('ZOOM_API_INTEGRATION_TABLE', 'zoom_api_integration');
define('ZOOM_MEETING_QUEUE_TABLE', 'zoom_meeting_queue');

/**
* ZoomAPIIntegrationInstall 
*/
class ZoomAPIIntegrationInstall {
  const ZAII_DB_VERSION = '1.0.10';
  
  public static function install() {
    self::create_database_table();
  }

  public static function create_database_table() {
    global $wpdb;

    $installed_ver = get_option( "zaii_db_version" );

		$wpdb->hide_errors();

    $charset_collate = $wpdb->get_charset_collate();

    if ( $installed_ver != self::ZAII_DB_VERSION ) {
      $zoom_api_integration_table = $wpdb->get_blog_prefix() . ZOOM_API_INTEGRATION_TABLE;
      $zoom_meeting_queue_table = $wpdb->get_blog_prefix() . ZOOM_MEETING_QUEUE_TABLE;

      $sql = "
        CREATE TABLE {$zoom_api_integration_table} (
          id BIGINT(20) NOT NULL AUTO_INCREMENT,
          api_key VARCHAR(255) NOT NULL,
          secret_key VARCHAR(255) NOT NULL,
          email VARCHAR(255) NOT NULL,
          zoom_meeting_id VARCHAR(50) NOT NULL,
          zoom_meeting_url VARCHAR(255) NOT NULL,
          participants BIGINT(20) NOT NULL DEFAULT 0,
          participants_capacity BIGINT(20) NOT NULL DEFAULT 1000,
          update_date DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
          active_meeting BOOLEAN NOT NULL DEFAULT TRUE,
          PRIMARY KEY (id),
          KEY idx_participants (participants)
        ) $charset_collate;

        CREATE TABLE {$zoom_meeting_queue_table} (
          id BIGINT(20) NOT NULL AUTO_INCREMENT,
          user_id BIGINT(20) NOT NULL,
          update_date DATETIME NOT NULL,
          PRIMARY KEY (id),
          KEY idx_user_id (user_id)
        ) $charset_collate;
      ";

      require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

      dbDelta( $sql );

      update_option( "zaii_db_version", self::ZAII_DB_VERSION );
    }
  }

	public static function update_db_check() {
    $installed_ver = get_option( "zaii_db_version" );

		if ( $installed_ver != self::ZAII_DB_VERSION ) {
			self::install();
		}
	}
}