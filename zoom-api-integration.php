<?php
/*
* Plugin Name: 				Zoom API Integration
* Description: 				Integrates with Zoom API.
* Author: 					Victor Lobo
* Version: 					1.0.0
* Text Domain: 				zoom-api-integration
*/

date_default_timezone_set('America/Sao_Paulo');

require_once(plugin_dir_path(__FILE__) . 'Utils.php');
require_once(plugin_dir_path(__FILE__) . 'cron-jobs.php');
require_once(plugin_dir_path(__FILE__) . 'ZoomAPIIntegration.php');
require_once(plugin_dir_path(__FILE__) . 'ZoomAPIIntegrationInstall.php');
require_once(plugin_dir_path(__FILE__) . 'join_meeting_ajax.php');

register_activation_hook( __FILE__, array( 'ZoomAPIIntegrationInstall', 'install' ) );
register_activation_hook( __FILE__, 'create_cron_jobs' );
register_deactivation_hook( __FILE__, 'delete_cron_jobs' );