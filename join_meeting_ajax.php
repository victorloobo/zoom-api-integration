<?php
global $salt;
$salt = md5((new DateTime())->format('Y-m-d'));

// Add the JS
add_action( 'wp_enqueue_scripts', 'theme_name_scripts' );
// add_action( 'login_enqueue_scripts', 'theme_name_scripts' );
function theme_name_scripts() {
  global $salt;
  wp_enqueue_style( 'waiting-message', plugin_dir_url(__FILE__) . 'assets/css/waiting-message.css', array(), '1.0.1' );
  wp_enqueue_script( 'join-zoom-meeting', plugin_dir_url(__FILE__) . 'assets/js/join-meeting.js', array('jquery'), '1.0.7', true );
  wp_localize_script( 'join-zoom-meeting', 'MyAjax', array(
    'join_zoom_meeting_ajaxurl' => admin_url( 'admin-ajax.php' ),
    'security' => wp_create_nonce( $salt )
  ));
}

// The function that handles the AJAX request
add_action( 'wp_ajax_join_meeting', 'join_meeting_callback' );
function join_meeting_callback() {
  global $salt;
	check_ajax_referer( $salt, 'security' );

  ZoomAPIIntegration::redirectToZoomMeetingOrAddToQueue();

  die(); // this is required to return a proper result
}