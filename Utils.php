<?php
abstract class Utils {
  public static function generateJWTToken($iss, $secret_key, $expire_in = 1) {
    $header = json_encode([
      'typ' => 'JWT', 
      'alg' => 'HS256'
    ]);

    $header = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($header));
    
    $expire = new DateTime();
    $expire->modify("+$expire_in minutes");

    $payload = json_encode([
      "iss" => $iss,
      "exp" => $expire->getTimestamp()
    ]);
    
    $payload = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($payload));
    
    $signature = hash_hmac('sha256', $header . "." . $payload, $secret_key, true);
    
    $signature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($signature));
    
    return "$header.$payload.$signature";
  }

  public static function currentDateTimeAsSting() {
    return (new DateTime())->format('Y-m-d H:i:s');
  }
}